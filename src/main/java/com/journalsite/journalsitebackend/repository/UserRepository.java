package com.journalsite.journalsitebackend.repository;

import com.journalsite.journalsitebackend.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    
}
