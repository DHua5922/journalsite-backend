package com.journalsite.journalsitebackend.controller;

import com.journalsite.journalsitebackend.model.User;
import com.journalsite.journalsitebackend.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository repository;

    @PostMapping("/create")
    public User createUser(@RequestBody User newUser) {
        return repository.save(newUser);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        return repository.findById(id).get();
    }

    @PatchMapping("/{id}")
    public User updateUser(@PathVariable int id, @RequestBody User updatedUser) {
        User user = repository.findById(id).get();

        if(updatedUser.getEmail() != null)
            user.setEmail(updatedUser.getEmail());
        
        if(updatedUser.getUsername() != null)
            user.setUsername(updatedUser.getUsername());
        
        return repository.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        repository.deleteById(id);
    }
}
