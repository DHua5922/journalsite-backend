package com.journalsite.journalsitebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JournalsiteBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(JournalsiteBackendApplication.class, args);
	}

}
