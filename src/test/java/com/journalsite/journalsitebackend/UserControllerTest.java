package com.journalsite.journalsitebackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.journalsite.journalsitebackend.controller.UserController;
import com.journalsite.journalsitebackend.model.User;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserController controller;

    @Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

    @Test
    public void createUser() throws Exception {
        User newUser = new User(1, "DHua5922", "hua.dylan@gmail.com");

        when(controller.createUser(any(User.class))).thenReturn(newUser);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(newUser);

        this.mockMvc
            .perform(post("/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(json))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("DHua5922"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("hua.dylan@gmail.com"))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void getUser() throws Exception {
        User user = new User(1, "DHua5922", "hua.dylan@gmail.com");

        when(controller.getUser(1)).thenReturn(user);

        this.mockMvc
            .perform(get("/user/1"))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("DHua5922"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("hua.dylan@gmail.com"))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void updateUser() throws Exception {
        User updatedUser = new User(1, "DHua", "dylan.hua@gmail.com");

        when(controller.updateUser(anyInt(), any(User.class))).thenReturn(updatedUser);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(updatedUser);
        
        this.mockMvc
            .perform(patch("/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(json))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("DHua"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("dylan.hua@gmail.com"))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void deleteUser() throws Exception {
        this.mockMvc
            .perform(delete("/user/1"))
            .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print());
    }
}
